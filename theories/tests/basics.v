(** Testing basic connectives *)
From iris_c.vcgen Require Import proofmode.

Section test.
  Context `{cmonadG Σ}.

  (** dereferencing *)
  Lemma test1 cl v :
    cl ↦C v -∗ CWP ∗ᶜ ♯ₗcl {{ w, ⌜w = v⌝ ∗ cl ↦C v }}.
  Proof. iIntros. vcg. auto. Qed.

  (** double dereferencing *)
  Lemma test2 cl1 cl2 v :
    cl1 ↦C cloc_to_val cl2 -∗ cl2 ↦C v -∗
    CWP ∗ᶜ ∗ᶜ ♯ₗcl1 {{ v, ⌜v = #1⌝ ∗ cl1 ↦C cloc_to_val cl2 -∗ cl2 ↦C v }}.
  Proof. iIntros. vcg. auto. Qed.

  (** sequence points *)
  Lemma test3 cl v :
    cl ↦C v -∗ CWP ∗ᶜ ♯ₗcl ;ᶜ ∗ᶜ ♯ₗcl {{ w, ⌜w = v⌝ ∗ cl ↦C v }}.
  Proof. iIntros. vcg. auto. Qed.

  (** assignments *)
  Lemma test4 (l : cloc) (v1 v2: val) :
    l ↦C v1 -∗ CWP ♯ₗl =ᶜ c_ret v2 {{ v, ⌜v = v2⌝ ∗ l ↦C[LLvl] v2 }}.
  Proof. iIntros. vcg. auto. Qed.

  Lemma store_load s l R :
    s ↦C #0 -∗ l ↦C #1 -∗
    CWP ♯ₗs =ᶜ ∗ᶜ ♯ₗl @ R {{ _, s ↦C[LLvl] #1 ∗ l ↦C #1 }}.
  Proof. iIntros. vcg. auto with iFrame. Qed.

  Lemma store_load_load s1 s2 l R :
    s1 ↦C #0 -∗ l ↦C #1 -∗ s2 ↦C #0 -∗
    CWP ♯ₗs1 =ᶜ ∗ᶜ ♯ₗl ;ᶜ ∗ᶜ ♯ₗs1 +ᶜ ♯42 @ R {{ _, s1 ↦C #1 ∗ l ↦C #1 }}.
  Proof. iIntros. vcg. auto with iFrame. Qed.

  (** double dereferencing & modification *)
  Lemma test5 (l1 l2 r1 r2 : cloc) (v1 v2: val) :
    l1 ↦C cloc_to_val l2 -∗ l2 ↦C v1 -∗
    r1 ↦C cloc_to_val r2 -∗ r2 ↦C v2 -∗
    CWP ♯ₗl1 =ᶜ ♯ₗr1 ;ᶜ ∗ᶜ ∗ᶜ ♯ₗl1
    {{ w, ⌜w = v2⌝ ∗
      l1 ↦C cloc_to_val r2 ∗ l2 ↦C v1 ∗ r1 ↦C cloc_to_val r2 -∗ r2 ↦C v2 }}.
  Proof. iIntros. vcg. auto with iFrame. Qed.

  (** par *)
  Lemma test_par_1 (l1 l2 : cloc) (v1 v2: val) :
    l1 ↦C v1 -∗ l2 ↦C v2 -∗
    CWP ∗ᶜ ♯ₗl1 |||ᶜ  ∗ᶜ ♯ₗl2
    {{ w, ⌜w = (v1, v2)%V⌝ ∗ l1 ↦C v1 ∗ l2 ↦C v2 }}.
  Proof. iIntros. vcg. auto with iFrame. Qed.

  Lemma test_par_2 (l1 l2 : cloc) (v1 v2: val) :
    l1 ↦C v1 -∗ l2 ↦C v2 -∗
    CWP (♯ₗl1 =ᶜ c_ret v2) |||ᶜ  (♯ₗl2 =ᶜ c_ret v1)
    {{ w, ⌜w = (v2, v1)%V⌝ ∗ l1 ↦C[LLvl] v2 ∗ l2 ↦C[LLvl] v1 }}.
  Proof. iIntros. vcg. auto with iFrame. Qed.

  (** pre bin op *)
  Lemma test6 (l : cloc) (z0 : Z) R:
    l ↦C #z0 -∗
    CWP ♯ₗl +=ᶜ ♯1 @ R {{ v, ⌜v = #z0⌝ ∧ l ↦C[LLvl] #(1 + z0) }}.
  Proof. iIntros. vcg. eauto with iFrame. Qed.

  Lemma test7 (l k : cloc) (z0 z1 : Z) R:
    l ↦C #z0 -∗
    k ↦C #z1 -∗
    CWP (♯ₗl +=ᶜ ♯1) +ᶜ (∗ᶜ♯ₗk) @ R
      {{ v, ⌜v = #(z0+z1)⌝ ∧ l ↦C[LLvl] #(1 + z0) ∗ k ↦C #z1 }}.
  Proof. iIntros. vcg. eauto with iFrame. Qed.

  (** more sequences *)
  Lemma test_seq s l :
    s ↦C[ULvl] #0 -∗ l ↦C[ULvl] #1 -∗
    CWP (♯ₗl =ᶜ ♯2 ;ᶜ ♯1 +ᶜ (♯ₗ l =ᶜ ♯1)) +ᶜ (♯ₗ s =ᶜ ♯4)
      {{ v, ⌜v = #6⌝ ∧ s ↦C[LLvl] #4 ∗ l ↦C[LLvl] #1 }}.
  Proof. iIntros. vcg. eauto with iFrame. Qed.

  Lemma test_seq2 s l :
    s ↦C[ULvl] #0 -∗ l ↦C[ULvl] #1 -∗
    CWP (♯ₗl =ᶜ ♯2 ;ᶜ ∗ᶜ ♯ₗl) +ᶜ (♯ₗs =ᶜ ♯4) {{ v, ⌜v = #6⌝ ∧ s ↦C[LLvl] #4 ∗ l ↦C #2 }}.
  Proof. iIntros. vcg. eauto with iFrame. Qed.

  Lemma test_seq3 l :
    l ↦C #0 -∗
    CWP ♯ₗl =ᶜ ♯2 ;ᶜ ♯1 +ᶜ (♯ₗl =ᶜ ♯1) {{ _, l ↦C[LLvl] #1 }}.
  Proof. iIntros. vcg. eauto with iFrame. Qed.

  Lemma test_seq4 l k :
    l ↦C #0 -∗
    k ↦C #0 -∗
    CWP (♯ₗl =ᶜ ♯2 ;ᶜ ♯1 +ᶜ (♯ₗl =ᶜ ♯1)) +ᶜ (♯ₗk =ᶜ ♯2 ;ᶜ ♯1 +ᶜ (♯ₗk =ᶜ ♯1))
      {{ v, ⌜v = #4⌝ ∧ l ↦C[LLvl] #1 ∗ k ↦C[LLvl] #1 }}.
  Proof. iIntros. vcg. eauto with iFrame. Qed.

  Definition stupid (l : cloc) : expr :=
    ♯ₗ l =ᶜ ♯ 1;ᶜ c_ret #0.

  Lemma test_seq_fail l :
    l ↦C[ULvl] #0 -∗
    CWP (stupid l +ᶜ stupid l) +ᶜ (c_ret #0) {{ v, l ↦C #1 }}.
  Proof. iIntros. vcg. Fail by eauto with iFrame. Abort.

  Lemma test_seq5 l k :
    l ↦C #0 -∗
    k ↦C #0 -∗
    CWP ♯0 +ᶜ (♯ₗl =ᶜ ♯1 ;ᶜ ♯ₗk =ᶜ ♯2 ;ᶜ ♯0) {{ v, ⌜v = #0⌝ ∗ l ↦C #1 ∗ k ↦C #2 }}.
  Proof. iIntros. vcg. eauto with iFrame. Qed.

  Lemma test_seq6 l k :
    l ↦C #0 -∗
    k ↦C #0 -∗
    CWP ♯1 +ᶜ (♯ₗl =ᶜ ♯1 ;ᶜ (♯ₗk =ᶜ ♯2) +ᶜ ∗ᶜ ♯ₗl ;ᶜ ∗ᶜ ♯ₗk +ᶜ (♯ₗl =ᶜ ♯2))
      {{ v, ⌜v = #5⌝ ∗ l ↦C[LLvl] #2 ∗ k ↦C #2 }}.
  Proof. iIntros. vcg. eauto with iFrame. Qed.

  Lemma test_seq7 l :
    l ↦C #0 -∗
    CWP ♯1 +ᶜ (♯ₗl =ᶜ ♯1 ;ᶜ ∗ᶜ ♯ₗl +ᶜ ∗ᶜ ♯ₗl ;ᶜ ♯ₗl =ᶜ ♯2) {{ v, ⌜v = #3⌝ ∗ l ↦C[LLvl] #2 }}.
  Proof. iIntros. vcg. eauto with iFrame. Qed.

  (** while *)
  Lemma test_while l R :
    l ↦C #1 -∗
    CWP whileᶜ (∗ᶜ ♯ₗl <ᶜ ♯2) { ♯ₗl =ᶜ ♯1 } @ R {{ _, True }}.
  Proof.
    iIntros. vcg. iIntros. iLöb as "IH".
    iApply cwp_whileV; iNext.
    vcg. iIntros "Hl".
    iLeft. iSplitR; eauto. iModIntro.
    vcg. iIntros "Hl". iModIntro. by iApply "IH".
  Qed.
End test.
