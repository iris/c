From iris_c.vcgen Require Import proofmode.

Section swap.
  Context `{cmonadG Σ}.

  Definition swap : val := λᶜ "a",
    "l1" ←ᶜ c_ret (Fst "a");ᶜ
    "l2" ←ᶜ c_ret (Snd "a");ᶜ
    "r" ←mutᶜ ∗ᶜ (c_ret "l1") ;ᶜ
    (c_ret "l1") =ᶜ ∗ᶜ (c_ret "l2") ;ᶜ
    (c_ret "l2") =ᶜ ∗ᶜ (c_ret "r") ;ᶜ
    ♯().

  Lemma swap_spec l1 l2 v1 v2 R :
    l1 ↦C v1 -∗ l2 ↦C v2 -∗
    CWP swap (cloc_to_val l1, cloc_to_val l2)%V @ R {{ _, l2 ↦C v1 ∗ l1 ↦C v2 }}.
  Proof. iIntros. iApply cwp_fun; simpl. vcg. eauto with iFrame. Qed.

  Definition swap_with_alloc : val := λᶜ "a",
    "l1" ←ᶜ c_ret (Fst "a");ᶜ
    "l2" ←ᶜ c_ret (Snd "a");ᶜ
    "r" ←ᶜ allocᶜ (♯1, ♯0);ᶜ
    (c_ret "r")  =ᶜ ∗ᶜ (c_ret "l1") ;ᶜ
    (c_ret "l1") =ᶜ ∗ᶜ (c_ret "l2") ;ᶜ
    (c_ret "l2") =ᶜ ∗ᶜ (c_ret "r") ;ᶜ
    ♯().

  Lemma swap_with_alloc_spec l1 l2 v1 v2 R :
    l1 ↦C v1 -∗ l2 ↦C v2 -∗
    CWP swap_with_alloc (cloc_to_val l1, cloc_to_val l2)%V @ R {{ _, l1 ↦C v2 ∗ l2 ↦C v1 }}.
  Proof. iIntros. iApply cwp_fun; simpl. vcg; eauto with iFrame. Qed.
End swap.
