From iris_c.vcgen Require Import proofmode.

Section test.
  Context `{cmonadG Σ}.

  Lemma ptr_plus_test1 p q n v1 R Φ :
    Φ (cloc_to_val (cloc_plus p n)) -∗
    p ↦C v1 -∗
    q ↦C cloc_to_val p -∗
    CWP ∗ᶜ♯ₗq +∗ᶜ ♯n @ R {{ v, Φ v ∗ p ↦C v1 ∗ q ↦C cloc_to_val p }}.
  Proof. iIntros. vcg. eauto with iFrame. Qed.
End test.
