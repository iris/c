From stdpp Require Import namespaces.
From iris_c.vcgen Require Import proofmode.
From iris_c.lib Require Import mset list.
From iris.algebra Require Import lib.excl_auth.

(** This example is meant to demonstrate the usage of atomic rules for
the primitive operations, as well as for function calls.  In this file
we verify the following program:

    #include <stdlib.h>
    #include <stdio.h>

    int storeme(int * l) { *l = 10; return 10; }

    int main() {
      int * l = calloc(sizeof(int), 1);
      int r = storeme(l) + ( *l = 11 );
      printf("*l = %d, r = %d\n", *l, r);
      return 0;
    }

We prove that the result value r is always 21 and that the value store in l
is either 10 or 11.

To prove this specification we use an invariant that tracks whether the
left/right hand side of the plus operator has been executed. This is achived
using the ghost state variables `γl` and `γr`. The invariant guarantees that
if `γl ≔ b1 ∗ γr ≔ b2`, then we have four possible situations by case-analysis.

    match b1, b2 with
    | false, false => l ↦C #0
    | true, false => l ↦C #10
    | false, true => l ↦C[LLvl] #11
    | true, true => l ↦C #10 ∨ l ↦C[LLvl] #11
    end.

*)

Definition storeme : val := λᶜ "l", c_ret "l" =ᶜ ♯ 10.

Definition test : val := λᶜ "l",
  callᶜ (c_ret storeme) (c_ret "l") +ᶜ (c_ret "l" =ᶜ ♯11).

Section test.
  Context `{cmonadG Σ, !inG Σ (authR (optionUR (exclR boolO)))}.

  (** Basic specification for `storeme' *)
  Lemma storeme_spec R cl v Φ :
    cl ↦C v -∗ (cl ↦C #10 -∗ Φ #10) -∗
    CWP storeme (cloc_to_val cl) @ R {{ Φ }}.
  Proof.
    iIntros "? H". iApply cwp_fun; simpl. vcg; iIntros "? !>". by iApply "H".
  Qed.

   (** The correctness of the test function. *)
  Definition test_inv cl γl γr : iProp Σ := (∃ b1 b2,
    own γl (●E b1) ∗ own γr (●E b2) ∗
    match b1, b2 with
    | false, false => cl ↦C #0
    | true, false => cl ↦C #10
    | false, true => cl ↦C[LLvl] #11
    | _, _ => cl ↦C #10 ∨ cl ↦C[LLvl] #11
    end)%I.

  Lemma test_spec R cl `{inG Σ testR, inG Σ fracR} :
    cl ↦C #0 -∗
    CWP "x" ←ᶜ test (cloc_to_val cl);ᶜ c_ret "x" @ R {{ v, ⌜ v = #21 ⌝ ∧
        (cl ↦C #10 ∨ cl ↦C #11) }}.
  Proof.
    iIntros "Hl". iApply cwp_seq_bind. iApply cwp_fun. simpl.
    iMod (own_alloc (●E false ⋅ ◯E false)) as (γl) "[H1 lb]"; first by apply excl_auth_valid.
    iMod (own_alloc (●E false ⋅ ◯E false)) as (γr) "[H2 rb]"; first by apply excl_auth_valid.
    iApply (cwp_insert_res _ _ (test_inv cl γl γr) with "[H1 H2 Hl]").
    { iNext. iExists false,false. iFrame. }
    iApply (cwp_bin_op _ _ (λ v, ⌜v = #10⌝ ∗ own γl (◯E true))%I
                           (λ v, ⌜v = #11⌝ ∗ own γr (◯E true))%I
              with "[lb] [rb]").
    - vcg. unfold test_inv. iIntros "[H R]".
      iDestruct "H" as (b1 b2) "(H1 & H2 & H)".
      iDestruct (own_valid_2 with "H1 lb") as %->%excl_auth_agree_L.
      destruct b2; iNext; iModIntro.
      + iMod (own_update_2 with "H1 lb") as "[H1 lb]";
          first by apply (excl_auth_update _ _ true).
        iApply (storeme_spec with "H").
        iIntros "Hl". iFrame "R".
        iSplitR "lb"; first by (iExists _,_; eauto with iFrame).
        vcg_continue; auto.
      + iMod (own_update_2 with "H1 lb") as "[H1 lb]";
          first by apply (excl_auth_update _ _ true).
        iApply (storeme_spec with "H").
        iIntros "Hl". iFrame "R".
        iSplitR "lb"; first by (iExists _,_; eauto with iFrame).
        vcg_continue; auto.
    - iApply (cwp_store _ _ (λ v, ⌜v = cloc_to_val cl⌝)%I
                            (λ v, ⌜v = #11⌝)%I).
      1,2: vcg; eauto.
      iIntros (? ? -> ->) "[H R]". unfold test_inv.
      iDestruct "H" as (b1 b2) "(H1 & H2 & H)".
      iDestruct (own_valid_2 with "H2 rb") as %->%excl_auth_agree_L.
      iModIntro.
      destruct b1; iEval (simpl) in "H".
      + iExists cl, _. iFrame. iSplit; first done.
        iIntros "Hl".
        iMod (own_update_2 with "H2 rb") as "[H2 rb]";
          first by apply (excl_auth_update _ _ true).
        iModIntro. iSplitR "rb"; last by eauto with iFrame.
        iExists _,_; eauto with iFrame.
      + iExists cl, _. iFrame. iSplit; first done.
        iIntros "Hl".
        iMod (own_update_2 with "H2 rb") as "[H2 rb]";
          first by apply (excl_auth_update _ _ true).
        iModIntro. iSplitR "rb"; last by eauto with iFrame.
        iExists _,_; eauto with iFrame.
    - iIntros (v1 v2) "[% lb] [% rb]"; simplify_eq/=.
      iExists #21; simpl. iSplit; first done.
      iIntros "H". iDestruct "H" as (b1 b2) "(H1 & H2 & H)".
      do 3 iModIntro.
      iDestruct (own_valid_2 with "lb H1") as %->%excl_auth_agree_L.
      iDestruct (own_valid_2 with "rb H2") as %->%excl_auth_agree_L.
      iDestruct "H" as "[H|H]"; iModIntro; vcg; eauto with iFrame.
  Qed.
End test.
