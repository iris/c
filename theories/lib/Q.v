From iris.algebra Require Export frac.
From Coq Require Export QArith Qcanon.

Lemma Q_plus_nonneg p q : (0 < p)%Q → (0 < q)%Q → (0 < p + q)%Q.
Proof.
  intros. apply Qlt_trans with p=> //. by rewrite -{1}(Qplus_0_r p) Qplus_lt_r.
Qed.
Lemma Q_div_nonneg p q : (0 < p)%Q → (0 < q)%Q → (0 < p / q)%Q.
Proof. intros. by apply Qlt_shift_div_l; rewrite ?Qmult_0_l. Qed.

Instance Q_eq_dec : EqDecision Q.
Proof. solve_decision. Defined.
Instance Q_Qeq_dec : RelDecision Qeq.
Proof. intros pq; apply Qeq_dec. Defined.
Instance Q_lt_dec : RelDecision Qlt.
Proof. refine (λ p q, cast_if (Qlt_le_dec p q)); auto using Qle_not_lt. Defined.

Definition Qp_to_Q (q : Qp) : Q := Qp_to_Qc q.

Definition Q_to_Qp (q : Q) : Qp :=
  match Qred q with
  | Qmake (Zpos n) 1 => pos_to_Qp n
  | Qmake (Zpos n) d => pos_to_Qp n / pos_to_Qp d
  | _ => 1 (* dummy *)
  end%Qp.
Arguments Q_to_Qp !_ /.
Local Arguments Q_to_Qp : simpl never.

Lemma Q_to_Qp_le_0 q : ¬(0 < q)%Q → Q_to_Qp q = 1%Qp.
Proof.
  destruct q as [[|n|n] d]=> //=.
  rewrite /Q_to_Qp /Qred /=. by destruct (Pos.ggcd _ _) as [? [??]].
Qed.
Lemma Q_to_Qp_1 : Q_to_Qp 1 = 1%Qp.
Proof. done. Qed.

Lemma Qp_to_Q_nonneg q : (0 < Qp_to_Q q)%Q.
Proof. by destruct q. Qed.

Lemma Q_of_to_Qp q : (0 < q)%Q → Qp_to_Q (Q_to_Qp q) == q.
Proof.
  rewrite -{1 3}(Qred_correct q) /Q_to_Qp.
  destruct (Qred q) as [[|n|n] [d|d|]]=> ? //=.
  - by rewrite Qmake_Qdiv /Qp_to_Q /pos_to_Qp /Qc_of_Z /= !Qred_correct.
  - by rewrite Qmake_Qdiv /Qp_to_Q /pos_to_Qp /Qc_of_Z /= !Qred_correct.
Qed.

Lemma Q_to_of_Qp (q : Qp) : Q_to_Qp (Qp_to_Q q) = q.
Proof. apply Qp_to_Qc_inj_iff, Qc_is_canon, Q_of_to_Qp, Qp_to_Q_nonneg. Qed.

Instance Q_to_Qp_proper : Proper (Qeq ==> (=)) Q_to_Qp.
Proof. rewrite /Q_to_Qp. by intros p p' ->%Qred_complete. Qed.
Instance Qp_to_Q_inj : Inj (=) Qeq Qp_to_Q.
Proof. intros p q Hpq. by rewrite -(Q_to_of_Qp p) -(Q_to_of_Qp q) Hpq. Qed.

Lemma Qp_to_Q_add p q : Qp_to_Q (p + q) == Qp_to_Q p + Qp_to_Q q.
Proof. destruct p, q. by rewrite /Qp_to_Q /= Qred_correct. Qed.
Lemma Qp_to_Q_div p q : Qp_to_Q (p / q) == Qp_to_Q p / Qp_to_Q q.
Proof. destruct p, q. by rewrite /Qp_to_Q /= !Qred_correct. Qed.

Lemma Q_to_Qp_add p q :
  (0 < p)%Q → (0 < q)%Q → Q_to_Qp (p + q) = (Q_to_Qp p + Q_to_Qp q)%Qp.
Proof.
  intros. assert (0 < p + q)%Q by eauto using Q_plus_nonneg.
  apply (inj Qp_to_Q). by rewrite Qp_to_Q_add !Q_of_to_Qp.
Qed.
Lemma Q_to_Qp_div p q :
  (0 < p)%Q → (0 < q)%Q → Q_to_Qp (p / q) = (Q_to_Qp p / Q_to_Qp q)%Qp.
Proof.
  intros. assert (0 < p / q)%Q by eauto using Q_div_nonneg.
  apply (inj Qp_to_Q). by rewrite Qp_to_Q_div !Q_of_to_Qp.
Qed.
